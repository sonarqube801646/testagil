class MathOperations:
    @staticmethod
    def add (x,y):
        return x+y
    
    @staticmethod
    def multiply (x,y):
        return x*y

sum_result = MathOperations.add(3,4)
product_result= MathOperations.multiply(3,4)

# ---- Print results ----
# print("Sum Result:", sum_result)
# print("Product Result:", product_result)