import unittest

from MathOperations import MathOperations

class TestMathOperations(unittest.TestCase):

    def test_add(self):
        result = MathOperations.add(3, 4)
        self.assertEqual(result, 7, "La somme de 3 et 4 devrait être égale à 7")

    def test_multyply(self):
         result = MathOperations.multiply(2, 5)
         self.assertEqual(result, 10, "Le produit de 2 et 5 devrait être égale à 10")

if __name__ == '__main__':
    unittest.main()
